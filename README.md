# salmon-and-groundwater

Contains supporting information for Larsen, L.G. and Woelfle-Erskine, C. (In review, 2018). Groundwater is key to salmonid persistence and recruitment in intermittent Mediterranean-climate streams. In review at Water Resources Research.